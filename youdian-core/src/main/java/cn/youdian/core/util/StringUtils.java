package cn.youdian.core.util;

import java.util.Random;

/**
 * 字符串工具库
 *
 * @author K.J
 * @date 2021.05.30 23:07:01
 */
public class StringUtils {

    /**
     * 随机字符串(包含数字)
     * @param count
     * @return
     */
    public static String random(int count) {
        String str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < count; i++) {
            int number = random.nextInt(62);
            sb.append(str.charAt(number));
        }
        return sb.toString();
    }
}
