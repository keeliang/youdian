package cn.youdian.core.api;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author K.J
 * @date 2021.05.29 18:21:07
 */
public class JsonResult<T> {

    @JsonProperty("code")
    private int code;

    @JsonProperty("msg")
    private String msg;

    @JsonProperty("result")
    private T result;

    public JsonResult(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public JsonResult() {
        this.code = ResultCode.FAIL;
    }

    public JsonResult(int code, String msg, T result) {
        this.code = code;
        this.msg = msg;
        this.result = result;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }
}
