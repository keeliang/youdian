package cn.youdian.core.api;

/**
 * 状态码
 *
 * @author K.J
 * @date 2021.05.31 21:29:22
 */
public class ResultCode {

    public static final int SUCCESS = 0;

    public static final int FAIL = 1;

    public static final int ERROR = 500;
}
