package cn.youdian.core.api.util;

import cn.youdian.core.api.JsonResult;
import cn.youdian.core.api.ResultCode;

/**
 * JsonResult工具库
 *
 * @author K.J
 * @date 2021.05.31 21:39:31
 */
public class JsonResultUtils {

    public static JsonResult ok() {
        return ok(null);
    }

    public static <T> JsonResult ok(T object) {
        // if (object instanceof PageModel) {
        // PageModel pageModel = (PageModel) object;
        // return new JsonResult(ResultCode.SUCCESS,
        // Constants.ResultCodeEnum.SUCCESS.getMessage(),
        // pageModel.getRecords(), pageModel.getTotal());
        // }
        if (object == null) {
            return new JsonResult(ResultCode.SUCCESS, null);
        }
        return new JsonResult(ResultCode.SUCCESS, null, object);
    }


    public static <T> JsonResult ok(String message, T object) {
        if (object == null) {
            return new JsonResult(ResultCode.SUCCESS, message);
        }
        return new JsonResult(ResultCode.SUCCESS, message, object);
    }


    public static JsonResult fail(int code, String message) {
        return new JsonResult(code, message);
    }


//    public static JsonResult fail(int code) {
//        return fail(code, Resources.getMessage("STATUS_CODE_" + code));
//    }
}
