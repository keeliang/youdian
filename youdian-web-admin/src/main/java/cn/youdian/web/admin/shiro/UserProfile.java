package cn.youdian.web.admin.shiro;

/**
 * 授权用户凭证
 *
 * @author K.J
 * @date 2021.05.31 00:15:54
 */
public class UserProfile {

    private String userName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
