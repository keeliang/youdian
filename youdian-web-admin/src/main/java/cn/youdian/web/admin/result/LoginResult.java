package cn.youdian.web.admin.result;

/**
 * 登录返回实体
 *
 * @author K.J
 * @date 2021.05.29 18:17:21
 */
public class LoginResult extends BaseResult{

    private String sid;

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }
}
