package cn.youdian.web.admin.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 用户表单实体
 *
 * @author K.J
 * @date 2021.05.30 23:01:57
 */
public class UserModel extends BaseModel {

    @JsonProperty("user_id")
    private String userId;

    @JsonProperty("user_name")
    private String userName;

    @JsonProperty("user_password")
    private String password;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
