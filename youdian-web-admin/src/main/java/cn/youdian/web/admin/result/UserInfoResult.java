package cn.youdian.web.admin.result;

/**
 * @author K.J
 * @description TODO
 * @date 2021/6/2 10:25
 **/
public class UserInfoResult extends BaseResult {

    /**
     * 用户名
     */
    private String userName;

    /**
     * 头像地址
     */
    private String avatarUrl;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }
}
