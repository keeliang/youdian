package cn.youdian.web.admin;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@MapperScan("cn.youdian.data.mapper")
@ComponentScan(basePackages = {"cn.youdian.*"})
public class YoudianWebAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(YoudianWebAdminApplication.class, args);
    }

}
