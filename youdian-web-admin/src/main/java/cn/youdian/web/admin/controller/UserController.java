package cn.youdian.web.admin.controller;

import cn.youdian.core.api.JsonResult;
import cn.youdian.core.api.util.JsonResultUtils;
import cn.youdian.core.util.StringUtils;
import cn.youdian.data.entity.User;
import cn.youdian.service.UserService;
import cn.youdian.web.admin.WorkContext;
import cn.youdian.web.admin.model.UserModel;
import cn.youdian.web.admin.result.UserInfoResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * 用户
 *
 * @author K.J
 * @date 2021.05.30 22:59:12
 */
@RequestMapping("/user")
@RestController
public class UserController extends BaseController {

    @Autowired
    private UserService userService;

    @ResponseBody
    @GetMapping(value = "/info")
    public JsonResult<UserInfoResult> info() {
        JsonResult<UserInfoResult> jsonResult = new JsonResult<>();
        String userName = WorkContext.CurrentUser().getUserName();
        UserInfoResult result = new UserInfoResult();
        result.setUserName(userName);
        jsonResult = JsonResultUtils.ok(result);
        return jsonResult;
    }

    @ResponseBody
    @GetMapping("/list")
    public JsonResult<String> list() {
        JsonResult<String> jsonResult = new JsonResult<>();

        return jsonResult;
    }

    @ResponseBody
    @PostMapping(value = "/save")
    public JsonResult<Long> save(@RequestBody UserModel model) {
        JsonResult<Long> jsonResult = new JsonResult<>();

        if (!org.springframework.util.StringUtils.hasLength(model.getUserId())) {
            User user = new User();
            user.setSalt(StringUtils.random(6));
            user.setPassword(model.getPassword());
            user.setUserName(model.getUserName());
            Long userId = userService.insert(user);
        } else {

        }
        return jsonResult;
    }

}
