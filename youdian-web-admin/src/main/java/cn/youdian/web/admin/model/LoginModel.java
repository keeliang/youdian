package cn.youdian.web.admin.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 登录实体
 *
 * @author K.J
 * @date 2021.05.29 16:40:33
 */
public class LoginModel extends BaseModel {

    @JsonProperty("user_name")
    private String username;

    @JsonProperty("password")
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
