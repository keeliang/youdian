package cn.youdian.web.admin.shiro;

import org.apache.shiro.authc.UsernamePasswordToken;

/**
 * @author K.J
 * @date 2021.05.31 17:35:23
 */
public class CustomToken extends UsernamePasswordToken {
    private String returnUrl;

    public CustomToken(String userId, String password, String returnUrl) {
        super(userId, password);
        this.returnUrl = returnUrl;
    }

    public String getReturnUrl() {
        return returnUrl;
    }
}