package cn.youdian.web.admin.config;

import cn.youdian.web.admin.shiro.AuthorizerRealm;
import cn.youdian.web.admin.shiro.CustomSessionManager;
import org.apache.shiro.session.mgt.SessionManager;
import org.apache.shiro.session.mgt.eis.SessionIdGenerator;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.servlet.Cookie;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Shiro配置
 *
 * @author K.J
 * @date 2021.05.31 00:07:20
 */
@Configuration
public class ShiroConfig {
    @Bean
    public AuthorizerRealm authorizeRealm() {
        return new AuthorizerRealm();
    }

    @Bean
    public DefaultWebSecurityManager securityManager(AuthorizerRealm authorizerRealm) {
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        securityManager.setRealm(authorizerRealm);

        //注入自定义sessionManager
        securityManager.setSessionManager(sessionManager());

        return securityManager;
    }

    //    @Bean
//    public ShiroFilterChainDefinition shiroFilterChainDefinition() {
//        DefaultShiroFilterChainDefinition chainDefinition = new DefaultShiroFilterChainDefinition();
//
//        // logged in users with the 'document:read' permission
//        chainDefinition.addPathDefinition("/docs/**", "authc, perms[document:read]");
//
//        chainDefinition.addPathDefinition("/login", "anon");
//        chainDefinition.addPathDefinition("/doLogin", "anon");
//
//        // all other paths require a logged in user
//        chainDefinition.addPathDefinition("/**", "authc");
//
//
//        return chainDefinition;
//    }

    public Cookie cookieDAO() {
        Cookie cookie=new org.apache.shiro.web.servlet.SimpleCookie();
        cookie.setName("yd_sessionid");
        return cookie;
    }

    //自定义sessionManager
    @Bean
    public SessionManager sessionManager() {
        CustomSessionManager sessionManager=  new CustomSessionManager();
        sessionManager.setSessionIdCookie(cookieDAO());
        return sessionManager;
    }

    @Bean(name = "shiroFilterFactoryBean")
    public ShiroFilterFactoryBean shiroFilter(ShiroProperties shiroProperties, DefaultWebSecurityManager securityManager) {
        System.out.println("ShiroFilterFactoryBean.shiroFilter");
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
        shiroFilterFactoryBean.setSecurityManager(securityManager);

        Map<String, String> filterChainDefinitionMap = new LinkedHashMap<String, String>();

        shiroProperties
                .getFilters()
                .forEach(
                        f -> {
                            String[] fs = f.split(":");
                            filterChainDefinitionMap.put(fs[0], fs[1]);
                        });

        shiroFilterFactoryBean.setLoginUrl(shiroProperties.getLoginUrl());
        shiroFilterFactoryBean.setSuccessUrl(shiroProperties.getSuccessUrl());
        shiroFilterFactoryBean.setUnauthorizedUrl(shiroProperties.getUnauthorizedUrl());
        shiroFilterFactoryBean.setFilterChainDefinitionMap(filterChainDefinitionMap);
        return shiroFilterFactoryBean;
    }
}
