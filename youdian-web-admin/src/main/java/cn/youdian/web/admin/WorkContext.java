package cn.youdian.web.admin;

import cn.youdian.web.admin.shiro.UserProfile;
import org.apache.shiro.SecurityUtils;

/**
 * @author K.J
 * @description TODO
 * @date 2021/6/2 10:31
 **/
public class WorkContext {

    /**
     * 当前登录用户信息
     * @return
     */
    public static UserProfile CurrentUser() {

        UserProfile user = (UserProfile) SecurityUtils.getSubject().getPrincipal();

        return user;
    }
}
