package cn.youdian.web.admin.controller;

import cn.youdian.web.admin.YoudianWebAdminApplicationTests;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import java.io.UnsupportedEncodingException;

/**
 * 用户控制器单元测试
 *
 * @author K.J
 * @date 2021.05.30 23:13:47
 */
public class UserControllerTest extends YoudianWebAdminApplicationTests {

    @Test
    public void save() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/user/save")
//                .param("name", "admin")
                .accept(MediaType.APPLICATION_JSON))

                // .andExpect(MockMvcResultMatchers.status().isOk())             //等同于Assert.assertEquals(200,status);
                // .andExpect(MockMvcResultMatchers.content().string("hello lvgang"))    //等同于 Assert.assertEquals("hello lvgang",content);
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
        int status = mvcResult.getResponse().getStatus();                 //得到返回代码
        System.out.println(status);
        String content = mvcResult.getResponse().getContentAsString();    //得到返回结果
        System.out.println(content);
    }
}
