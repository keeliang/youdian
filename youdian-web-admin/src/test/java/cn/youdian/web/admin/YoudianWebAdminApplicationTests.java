package cn.youdian.web.admin;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@SpringBootTest
@RunWith(SpringRunner.class)
public class YoudianWebAdminApplicationTests {


    @Autowired
    private WebApplicationContext webApplicationContext;

    protected MockMvc mockMvc;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

//
//    @Autowired
//    private UserMapper userMapper;
//
//    @Test
//    void contextLoads() {
//        User user=new User();
//        user.setUserName("Test");
//        user.setPassword("fadsfajow21312");
//        user.setCreateTs(new Date());
//        user.setLastLoginTs(new Date());
//        user.setSalt("342dfw2");
//        userMapper.insert(user);
//    }

}
