package cn.youdian.service;

import cn.youdian.web.admin.YoudianWebAdminApplication;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author K.J
 * @description TODO
 * @date 2021/6/8 10:08
 **/
@RunWith(SpringRunner.class)
@SpringBootTest(classes = YoudianWebAdminApplication.class)
public class BaseServiceTest {
}
