package cn.youdian.service;

import cn.youdian.data.entity.Product;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class ProductBaseServiceTest extends BaseServiceTest {

    @Autowired
    private ProductService productService;

    @Test
    public void insert() {
        Product product = new Product();
        product.setCategoryId(2l);
        product.setProductDesc("江阴仁心生物科技有限公司专业从事绿色中草药健康产业开发，是一家集原料收储、加工、提取和系列保健食品研发、生产、销售于体的现代化高科技企业。江阴仁心生物致力于为人类健康造福，使尘世中的人能够在人生百年旅途中感受到生命的美好。因此，创建伊始,河南神芸生物就确定了自己的使命:取天地之精华，造岁月之从容。肉苁蓉，又名大芸，是中华九大仙草之一。肉苁蓉在我国已有2000多年的药用历史，具有补肾、益精血、润肠通");
        product.setProductDetail("");
        product.setProductName("神芸康天地苁蓉-03");
        product.setProductThum("/assets/img/201911261729038021.jpg");
        System.out.println(productService.insert(product));
    }
}