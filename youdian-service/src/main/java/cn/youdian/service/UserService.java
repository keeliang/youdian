package cn.youdian.service;

import cn.youdian.data.entity.User;

/**
 * 用户服务
 *
 * @author K.J
 * @date 2021.05.29 21:24:42
 */
public interface UserService {

    /**
     * 添加用户
     *
     * @param user
     * @return
     */
    Long insert(User user);

    /**
     * 更新用户信息
     *
     * @param user
     */
    void update(User user);

    /**
     * 根据用户名获取用户信息
     *
     * @param userName
     * @return
     */
    User queryByUserName(String userName);

    /**
     * 用户登录验证
     *
     * @param userName
     * @param password
     * @return
     */
    User login(String userName, String password);

}
