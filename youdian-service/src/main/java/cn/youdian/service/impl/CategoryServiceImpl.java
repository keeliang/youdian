package cn.youdian.service.impl;

import cn.youdian.data.entity.Category;
import cn.youdian.data.mapper.CategoryMapper;
import cn.youdian.service.CategoryService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 分类服务实现
 *
 * @author K.J
 * @date 2021.06.07 23:11:10
 */
@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryMapper categoryMapper;

    @Override
    public List<Category> all() {
        LambdaQueryWrapper<Category> queryWrapper = Wrappers.<Category>lambdaQuery();
        queryWrapper.orderByAsc(true, Category::getId);
        return categoryMapper.selectList(queryWrapper);
    }
}
