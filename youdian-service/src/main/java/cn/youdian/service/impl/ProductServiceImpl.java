package cn.youdian.service.impl;

import cn.youdian.data.entity.Product;
import cn.youdian.data.mapper.ProductMapper;
import cn.youdian.service.ProductService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 产品/案例服务实现
 *
 * @author K.J
 * @date 2021.06.07 23:12:05
 */
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductMapper productMapper;

    @Override
    public List<Product> listByCategory(Long categoryId) {

        LambdaQueryWrapper<Product> queryWrapper = Wrappers.<Product>lambdaQuery();
        if (categoryId != 1) {
            queryWrapper.eq(Product::getCategoryId, categoryId);
        }
        return productMapper.selectList(queryWrapper);
    }

    @Override
    public Long insert(Product product) {
        product.setCreateTs(new Date());
        product.setLastTs(new Date());
        productMapper.insert(product);
        return product.getId();
    }
}
