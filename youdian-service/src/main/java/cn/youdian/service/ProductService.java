package cn.youdian.service;

import cn.youdian.data.entity.Product;

import java.util.List;

/**
 * 产品/案例服务接口
 *
 * @author K.J
 * @date 2021.06.07 23:11:41
 */
public interface ProductService {
    /**
     * 指定分类的产品/案例列表
     *
     * @param categoryId 分类ID
     * @return
     */
    List<Product> listByCategory(Long categoryId);

    /**
     *
     * @param product
     * @return
     */
    Long insert(Product product);
}
