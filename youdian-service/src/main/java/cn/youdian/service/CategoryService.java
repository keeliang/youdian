package cn.youdian.service;

import cn.youdian.data.entity.Category;

import java.util.List;

/**
 * 分类服务
 *
 * @author K.J
 * @date 2021.06.07 23:10:24
 */
public interface CategoryService {
    /**
     * 所有分类
     *
     * @return
     */
    List<Category> all();
}
