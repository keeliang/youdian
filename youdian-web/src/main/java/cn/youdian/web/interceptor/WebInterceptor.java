package cn.youdian.web.interceptor;

import cn.youdian.web.model.MenuModel;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * @author K.J
 * @date 2021.06.03 23:24:02
 */
@Component
public class WebInterceptor implements HandlerInterceptor {

    /**
     * 拦截
     * @param request
     * @param response
     * @param handler
     * @param modelAndView
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
        if (modelAndView != null) {
            ModelMap modelMap = modelAndView.getModelMap();
            List<MenuModel> menus = new ArrayList<>();

            menus.add(new MenuModel("首页","/"));
            menus.add(new MenuModel("案例","/products"));
            menus.add(new MenuModel("关于","/"));
            menus.add(new MenuModel("资讯","/"));
            menus.add(new MenuModel("联系","/contact"));

            modelMap.addAttribute("menus",menus);
            modelMap.addAttribute("title","优典品牌包装设计");
            modelMap.addAttribute("description","优典品牌包装设计");
            modelMap.addAttribute("keywords","优典品牌包装设计");


            modelMap.addAttribute("nav_logo","");
        }
    }
}
