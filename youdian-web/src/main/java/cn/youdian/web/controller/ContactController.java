package cn.youdian.web.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author K.J
 * @description TODO
 * @date 2021/6/7 14:14
 **/
@RestController
public class ContactController extends BaseController {

    @RequestMapping("/contact")
    public ModelAndView index(){
        ModelAndView view=new ModelAndView("contact");

        return view;
    }
}
