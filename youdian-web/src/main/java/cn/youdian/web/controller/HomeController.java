package cn.youdian.web.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;


/**
 * 首页
 *
 * @author K.J
 * @date 2021.06.03 22:03:21
 */
@RestController
public class HomeController extends BaseController {

    @GetMapping("/")
    public ModelAndView index() {
        ModelAndView view = new ModelAndView("index");


        return view;
    }
}
