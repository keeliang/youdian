package cn.youdian.web.model;

/**
 * @author K.J
 * @date 2021.06.07 23:52:22
 */
public class CategoryModel extends BaseModel {

    private Long categoryId;

    private String categoryName;

    private boolean actived;

    public CategoryModel() {
    }

    public CategoryModel(Long categoryId, String categoryName, boolean actived) {
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.actived = actived;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public boolean isActived() {
        return actived;
    }

    public void setActived(boolean actived) {
        this.actived = actived;
    }
}
