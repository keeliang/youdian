package cn.youdian.web.model;

/**
 * 菜单数据
 *
 * @author K.J
 * @date 2021.06.03 22:39:08
 */
public class MenuModel {

    private String name;

    private String url;

    public MenuModel() {

    }

    public MenuModel(String name, String url) {
        this.name = name;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
