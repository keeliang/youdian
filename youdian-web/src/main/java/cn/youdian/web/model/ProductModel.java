package cn.youdian.web.model;


/**
 * @author K.J
 * @date 2021.06.07 23:52:34
 */
public class ProductModel extends BaseModel {

    private Long productId;

    private Long categoryId;

    private String productName;

    private String productDesc;

    /**
     * 缩略图
     */
    private String productThum;

    public ProductModel() {
    }

    /**
     *
     * @param productId
     * @param categoryId
     * @param productName
     * @param productDesc
     * @param productThum
     */
    public ProductModel(Long productId, Long categoryId, String productName, String productDesc, String productThum) {
        this.productId = productId;
        this.categoryId = categoryId;
        this.productName = productName;
        this.productDesc = productDesc;
        this.productThum = productThum;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDesc() {
        return productDesc;
    }

    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }

    public String getProductThum() {
        return productThum;
    }

    public void setProductThum(String productThum) {
        this.productThum = productThum;
    }
}
