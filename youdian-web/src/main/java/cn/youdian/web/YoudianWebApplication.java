package cn.youdian.web;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@MapperScan("cn.youdian.data.mapper")
@ComponentScan(basePackages = {"cn.youdian.*"})
public class YoudianWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(YoudianWebApplication.class, args);
    }

}
