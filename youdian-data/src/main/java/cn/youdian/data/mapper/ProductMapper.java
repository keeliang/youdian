package cn.youdian.data.mapper;

import cn.youdian.data.entity.Product;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 产品/案例Mapper
 *
 * @author K.J
 * @date 2021.06.07 23:09:22
 */
public interface ProductMapper extends BaseMapper<Product> {
}
