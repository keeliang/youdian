package cn.youdian.data.mapper;

import cn.youdian.data.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 用户表Mapper类
 *
 * @author K.J
 * @date 2021.05.29 20:36:38
 */
public interface UserMapper extends BaseMapper<User> {
}
