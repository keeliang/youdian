package cn.youdian.data.mapper;

import cn.youdian.data.entity.Category;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 分类Mapper
 *
 * @author K.J
 * @date 2021.06.07 23:08:58
 */
public interface CategoryMapper  extends BaseMapper<Category> {
}
