package cn.youdian.data.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;

/**
 * 分类
 *
 * @author K.J
 * @date 2021.06.07 22:56:16
 */
@TableName("yd_category")
public class Category extends BaseEntity {


    @TableField("id")
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    @TableField("category_name")
    private String userName;

    @TableField("category_create_ts")
    private Date createTs;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getCreateTs() {
        return createTs;
    }

    public void setCreateTs(Date createTs) {
        this.createTs = createTs;
    }
}
