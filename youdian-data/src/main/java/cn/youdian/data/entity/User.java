package cn.youdian.data.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;

/**
 * 用户表
 *
 * @author K.J
 * @date 2021.05.29 20:29:28
 */
@TableName("yd_user")
public class User extends BaseEntity {

    @TableField("id")
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    @TableField("user_name")
    private String userName;

    @TableField("user_password")
    private String password;

    @TableField("user_salt")
    private String salt;

    @TableField("user_create_ts")
    private Date createTs;

    @TableField("user_last_login_ts")
    private Date lastLoginTs;

    @TableField("user_last_ts")
    private Date lastTs;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public Date getCreateTs() {
        return createTs;
    }

    public void setCreateTs(Date createTs) {
        this.createTs = createTs;
    }

    public Date getLastLoginTs() {
        return lastLoginTs;
    }

    public void setLastLoginTs(Date lastLoginTs) {
        this.lastLoginTs = lastLoginTs;
    }

    public Date getLastTs() {
        return lastTs;
    }

    public void setLastTs(Date lastTs) {
        this.lastTs = lastTs;
    }
}
