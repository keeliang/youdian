package cn.youdian.data.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;

/**
 * 产品/案例
 *
 * @author K.J
 * @date 2021.06.07 23:03:12
 */
@TableName("yd_product")
public class Product extends BaseEntity{

    @TableField("id")
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    @TableField("category_id")
    private Long categoryId;

    @TableField("product_name")
    private String productName;

    @TableField("product_desc")
    private String productDesc;

    /**
     * 缩略图
     */
    @TableField("product_thum")
    private String productThum;

    @TableField("product_detail")
    private String productDetail;

    @TableField("product_create_ts")
    private Date createTs;

    @TableField("product_last_ts")
    private Date lastTs;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDesc() {
        return productDesc;
    }

    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }

    public String getProductThum() {
        return productThum;
    }

    public void setProductThum(String productThum) {
        this.productThum = productThum;
    }

    public String getProductDetail() {
        return productDetail;
    }

    public void setProductDetail(String productDetail) {
        this.productDetail = productDetail;
    }

    public Date getCreateTs() {
        return createTs;
    }

    public void setCreateTs(Date createTs) {
        this.createTs = createTs;
    }

    public Date getLastTs() {
        return lastTs;
    }

    public void setLastTs(Date lastTs) {
        this.lastTs = lastTs;
    }
}
