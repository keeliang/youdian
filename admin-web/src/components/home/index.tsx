/**
 * 主页装饰
 */

import React from "react";

class Home extends React.Component<{}, {}, any> {
  render() {
    return (
      <div className="home-page">
        {/* 头部 */}
        <div className="home-header">
          <div className="home__logo yd-editor">
            
          </div>
          <div className="home__navs"></div>
        </div>
        <div className="home__scroll-view">

        </div>
        <div className="home__rich-text"></div>
      </div>
    );
  }
}

export default Home;
