import React from "react";

class FloatingMenu {
  render() {
    return <div className="floating_menu">
      <ul>
        <li><i className="iconfont icon-shangyi"></i></li>
        <li><i className="iconfont icon-xiayi"></i></li>
        <li><i className="iconfont icon-icon-bainji"></i></li>
      </ul>
    </div>;
  }
}

export default FloatingMenu;
