import { Home, Dashboard } from "./components";
import {
  HashRouter as Router,
  // matchPath,
  // useLocation,
  Route,
  RouteProps,
  Link,
  Switch,
} from "react-router-dom";
import { Menu } from "antd";

export interface RouterProps extends RouteProps {
  text: string;
  icon: string;
  url: string;
}

const routes: RouterProps[] = [
  {
    exact: true,
    url: "/",
    text: "仪表盘",
    icon: "",
    component: Dashboard,
  },
  {
    exact: true,
    url: "/home",
    text: "首页装修",
    icon: "",
    component: Home,
  },
];
//底部编辑

/**
 * 
 * @returns 
 */
const CreateRoutes = () => {
  return (
    <Router>
      <Switch>
        {routes.map((r, i) => {
          return <Route
            key={i}
            exact={r.exact}
            path={r.url}
            children={r.children}
            component={r.component}
            render={r.render}
          />;
        })}
      </Switch>
    </Router>
  );
};

/**
 * 
 * @returns 创建导航菜单
 */
const CreateMenus = () => {
  return (
    <Router>
      <Menu theme="dark" mode="inline" defaultSelectedKeys={["0"]}>
        {routes.map((r, i) => {
          return (
            <Menu.Item key={i} icon={r.icon}>
              <Link to={r.url}>{r.text}</Link>
            </Menu.Item>
          );
        })}
      </Menu>
    </Router>
  );
};

// export default routes;

export { CreateRoutes, CreateMenus };
