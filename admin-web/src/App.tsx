import "./App.sass";
import "antd/dist/antd.css";
import "./components/style/index.sass";
import React, { useState } from "react";
import { Layout } from "antd";
import { CreateRoutes, CreateMenus } from "./router";
import { MenuUnfoldOutlined, MenuFoldOutlined } from "@ant-design/icons";

// import {
//   HashRouter as Router,
//   Switch,
// } from "react-router-dom";

const { Header, Sider, Content } = Layout;

const App = () => {
  const [collapsed] = useState(false);

  const toggle = () => {};

  return (
    <div className="App">
      <Layout>
        <Sider trigger={null} collapsible collapsed={collapsed}>
          <div className="logo">优典包装设计</div>
          <CreateMenus></CreateMenus>
        </Sider>
        <Layout className="site-layout">
          <Header className="site-layout-background" style={{ padding: 0 }}>
            {React.createElement(
              collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
              {
                className: "trigger",
                onClick: toggle,
              }
            )}
          </Header>
          <Content className="site-layout-background layout-content">
            {/* <Router>
              <Switch><CreateRoutes></CreateRoutes></Switch>
            </Router> */}
            <CreateRoutes></CreateRoutes>
          </Content>
        </Layout>
      </Layout>
    </div>
  );
};

export default App;
