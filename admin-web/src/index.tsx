import ReactDOM from "react-dom";
import "./index.sass";
import App from "./App";
// import reportWebVitals from './reportWebVitals';

ReactDOM.render(
  // <React.StrictMode>
  <>
    <link rel="stylesheet" href="http://at.alicdn.com/t/font_1612704_v0lv6u5vek.css"></link>
    <App />
  </>,
  //
  document.body
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();
